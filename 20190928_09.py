class Sign:

    def __init__(self, max_speed, id):
        self.max_speed = max_speed
        self.id = id

    def __str__(self):
        return 'Id: {}, speed limit: {}'.format(self.id, self.max_speed)


class Signs:
    def __init__(self, signs_list):
        self.signs_list = signs_list

    def __getitem__(self, item):
        return self.signs_list[item]

    def __repr__(self):
        for sign in self.signs_list:
            print('Id: {}, speed limit: {}'.format(sign.id, sign.max_speed))
        return ''

    def __len__(self):
        return len(self.signs_list)


if __name__ == '__main__':
    s1 = Sign(50, 1)
    s2 = Sign(90, 2)
    s3 = Sign(120, 3)
    s4 = Sign(80, 4)

    list1 = Signs([s1, s2, s3, s4])

    print(list1[2])

    print(len(list1))

