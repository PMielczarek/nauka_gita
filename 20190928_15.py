# Zad. 2
# Stwórzmy sobie symulację jazdy samochodu po drodze. Sprawdźmy na którym znaku ograniczenia prędkości samochód powinien
# dostać mandat. Samochód od początku porusza się ze stałą prędkością. W momencie przekroczenia prędkości samochód nie
# jedzie dalej oraz jest informacja jaka marka samochodu przekroczyła prędkość oraz na jakim znaku
# (wyświetlić unikalny identyfikator oraz maksymalną wartość prędkości)

import random


class Car:
    def __init__(self, brand, speed):
        self.brand = brand
        self.speed = speed

    def __str__(self):
        return "Car info:\nBrand: {}\nSpeed: {}".format(self.brand, self.speed)


class Sign:
    def __init__(self, ID, top_speed):
        self.ID = ID
        self.top_speed = top_speed

    def __str__(self):
        return "ID: {}, top speed: {}".format(self.ID, self.top_speed)


class Ride:
    def __init__(self, signs, car):
        self.signs = signs
        self.car = car

    def fine(self):
        random.shuffle(self.signs)
        for i in self.signs:
            if self.signs.index(i) == len(self.signs) - 1:
                print("You have driven the whole route without any fine!")
                break
            else:
                if self.car.speed < i.top_speed:
                    pass
                else:
                    print(
                        'You got fine! Sign {} says that the top speed is equal to'
                        ' {} and you had {} on speedometer!'.format(str(i.ID), i.top_speed, self.car.speed))
                    break


if __name__ == "__main__":
    car1 = Car("BMW", 150)
    car2 = Car("AUDI", 80)
    road1 = Sign(1, 140)
    road2 = Sign(2, 120)
    road3 = Sign(3, 100)
    road4 = Sign(4, 90)
    road5 = Sign(5, 60)
    road6 = Sign(6, 50)
    road7 = Sign(7, 70)

    list_of_signs = [road1, road2, road3, road4, road5, road6, road7]

    ride1 = Ride(list_of_signs, car2)

    ride1.fine()
