# zad domowe 3

class ID:

    def __init__(self, name, surname, pesel, ID):
        self.name = name
        self.surname = surname
        self.pesel = pesel
        self.ID = ID

    def __str__(self):
        return self.name + ' ' + self.surname + ' ' + self.pesel + ' ' + self.ID

class Person:

    def __init__(self, id):
            self.id = id

    def __str__(self):
        return self.id.__str__()


if __name__ == "__main__":
    docID = ID('Stefan', 'Nowak', '88120211148', 'ABC123456')
    person = Person(docID)
    print(person)
