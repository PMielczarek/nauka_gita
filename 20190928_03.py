from abc import ABC, abstractmethod

class Vehicle(ABC):

    @abstractmethod
    def start(self):
        pass

    @abstractmethod
    def stop(self):
        pass

class Car(Vehicle):

    def __init__(self, model, brand):
        self.model = model
        self.brand = brand
        self.currentSpeed = 0
        self.isMotorOn = False

    def start(self):
        if self.currentSpeed == 0:
            self.isMotorOn = True
        self.currentSpeed += 2

    def stop(self):
        if self.currentSpeed == 0:
            self.isMotorOn = False
        self.currentSpeed -= 2

    def turnOffMotor(self):
        self.isMotorOn = False

class Bicycle(Vehicle):
    def __init__(self, model, brand):
        self.model = model
        self.brand = brand
        self.currentSpeed = 0

    def start(self):
        self.currentSpeed += 1

    def stop(self):
        self.currentSpeed -= 1

car = Car("e36", "BMW")
bicycle = Bicycle("damka", "markaRower")

bicycle.start()
car.start()
print("Car start")
print(car.currentSpeed)
print("Bicycle start")
print(bicycle.currentSpeed)

car.__class__ = Vehicle
bicycle.__class__ = Vehicle

print("Car as Vehicle start")
car.start()
print("Bicycle as Vehicle start")
bicycle.start()
print(car.currentSpeed)
print(bicycle.currentSpeed)
print("Bicycle as Car start")
bicycle.__class__ = Car
#print(bicycle.isMotorOn)
bicycle.turnOffMotor()
print(bicycle.isMotorOn)
