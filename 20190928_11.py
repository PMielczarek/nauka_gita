"""
zróbcie funkcję która przyjmie zmienną ilość argumentów
"""


def manyArgs(*args):
    print(args)
    return args[0]


a = manyArgs(1, 2, 3)
b = manyArgs(1, 2, 3, 4, 5)
c = manyArgs(1, 2, 3, 4, 5, 6, 7)

print(a)
