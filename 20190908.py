class Characters:

    def __init__(self):
        self.hp = 50
        self.mp = 50

    def attack(self):
        pass

class Mage(Characters):
    def attack(self):
        self.mp -= 10


if __name__ == '__main__':
    character = Characters()
    magic = Mage()
    warrior = Characters()

    print(character.hp) # 50
    print(character.mp) # 50
    character.attack()
    print(character.hp) # 50
    print(character.mp) # 50
    print()
    print(magic.hp) # 50
    print(magic.mp) # 50
    magic.attack()
    print(magic.hp) # 50
    print(magic.mp) # 40
    print()
    print(warrior.hp) # 50
    print(warrior.mp) # 50

    warrior.attack()
    print(warrior.hp) # 50
    print(warrior.mp) # 50
    print()
