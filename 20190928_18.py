'''stwórz dokumentację dla klasy i metody i wyświetlić w mainie'''


class Something:
    """class_documentation"""
    pass

    def someMethod(self):
        """method_documentation"""
        pass


if __name__ == '__main__':
    obj = Something()
    print(obj.__doc__)
    print(obj.someMethod.__doc__)
