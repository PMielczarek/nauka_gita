'''
To teraz stwórzmy naszą klasę Pojazd na klasę czyto abstrakcyjną :)
Stwórz sobie ogólny interfejs dla pojazdu oraz stwórzmy klasy dziedziczące
Wyierzmy sobie 2

Każdy z nich po starcie ma inaczej zmienić prędkość (o inną wartość)
rower nie ma silnika, wiec nic dodatkowego przy starcie nie robi
natomiast samochód odpala silnik przy wywołaniu metody i zwiększa prędkość
'''

from abc import ABC, abstractmethod

class Vehicle(ABC):

    @abstractmethod
    def do_something(self):
        pass

    @abstractmethod
    def go(self):
        pass

class Car(Vehicle):
    def do_something(self):
        print('Hey')

    def go(self):
        print('Fuck')

class Bicycle(Vehicle):
    def do_something(self):
        print('Hey Hey')

    def go(self):
        print('Fuck Fuck')

if __name__ == "__main__":

    car = Car()
    bicycle = Bicycle()

    car.do_something()
    car.go()

    bicycle.do_something()
    bicycle.go()
