'''
Stwórzmy sobie także pracownika który zarabia pieniądze (mając pracę)

Pojawia się problem. Co jeśli nasz pracownik zarabia za godzinę?
Co jeśli jest na etacie?
Co Jeśli pensję dostaje tygodniową?
Skąd wiemy kto więcej zarabia?
'''

from abc import ABC, abstractmethod

class Employee(ABC):

    @abstractmethod
    def salary(self):
        pass

class EmployeeWeeklyWorking(Employee):
    def __init__(self, salaryPerOur, workingOurs):
        self.salaryPerOur = salaryPerOur
        self.workingOurs = workingOurs

    def salary(self):
        print(self.salaryPerOur * self.workingOurs)

class EmployeeMonthlyWorking(Employee):
    def __init__(self, salaryPerOur, workingOurs):
        self.salaryPerOur = salaryPerOur
        self.workingOurs = workingOurs

    def salary(self):
        print(self.salaryPerOur * self.workingOurs)


if __name__ == '__main__':
    employeeWeeklyWorking = EmployeeWeeklyWorking(44, 40)
    employeeWeeklyWorking1 = EmployeeWeeklyWorking(36, 38)
    employeeMonthlyWorking = EmployeeMonthlyWorking(25, 160)

    employeeWeeklyWorking.salary()
    employeeWeeklyWorking1.salary()
    employeeMonthlyWorking.salary()
