# Stwórzmy sobie znaki z max_speed oraz unikalnym ID

# oraz klasę Signs przechowującą ich kolekcję

# print(signs[2]) powinno zwrócić nam drugi znak

import random

class Sign:
    def __init__(self, top_speed):
        self.ID = str(random.randrange(0,9))+str(random.randrange(0,9))+str(random.randrange(0,9))
        self.top_speed = top_speed
    def __str__(self):
        return "ID: {}, top speed: {}".format(self.ID, self.top_speed)

class Signs:
    def __init__(self, list_of_signs):
        self.list_of_signs = list_of_signs

    def __getitem__(self, index):
        return self.list_of_signs[index]

if __name__ == "__main__":
    sign1 = Sign(140)
    sign2 = Sign(120)
    sign3 = Sign(100)
    sign4 = Sign(90)
    sign5 = Sign(60)
    sign6 = Sign(50)
    sign7 = Sign(70)

    list_of_signs = [sign1, sign2, sign3, sign4, sign5, sign6, sign7]

    signs = Signs(list_of_signs)

    print(signs[2])