"""
Tworzymy mały interfejsik dla gry
w której są różne characters (postacie).
Napisałem tutaj taki mały test do tego zadania,
takie wyniki powinny się znaleźć pod danymi wywołaniami.
PODPOWIEDŹ: jeśli do jakiejś zmiennej nie będziecie mogli się dostać
to użyjcie super z odpowiednim konstruktorem :)
usuń postać CHaracter z maina
"""
from abc import ABC, abstractmethod


class Characters(ABC):

    @abstractmethod
    def attack(self):
        pass


class Characters:

    def __init__(self):
        self.hp = 50
        self.mp = 50

    def attack(self):
        pass


class Mage(Characters):

    def attack(self):
        self.mp -= 10


if __name__ == '__main__':
    magic = Mage()
    warrior = Characters()

    print(magic.hp) # 50
    print(magic.mp) # 50
    magic.attack()
    print(magic.hp) # 50
    print(magic.mp) # 40
    print()

    print(warrior.hp) # 50
    print(warrior.mp) # 50
    warrior.attack()
    print(warrior.hp) # 50
    print(warrior.mp) # 50
    print()
