'''
Stwórzmy sobie także pracownika który zarabia pieniądze (mając pracę)

Pojawia się problem. Co jeśli nasz pracownik zarabia za godzinę?
Co jeśli jest na etacie?
Co Jeśli pensję dostaje tygodniową?
Skąd wiemy kto więcej zarabia?
Stwórzmy sobie zespół pracowników i zwróćmy jego zarobki na miesiąc
'''

from abc import ABC, abstractmethod

class Employee(ABC):

    @abstractmethod
    def salary(self):
        pass

class EmployeeWeeklyWorking1(Employee):
    def __init__(self, salaryPerOur, workingOurs):
        self.salaryPerOur = salaryPerOur
        self.workingOurs = workingOurs

    def salary(self):
        print(self.salaryPerOur * self.workingOurs)

class EmployeeWeeklyWorking2(Employee):
    def __init__(self, salaryPerOur, workingOurs):
        self.salaryPerOur = salaryPerOur
        self.workingOurs = workingOurs

    def salary(self):
        print(self.salaryPerOur * self.workingOurs)

class EmployeeMonthlyWorking1(Employee):
    def __init__(self, salaryPerOur, workingOurs):
        self.salaryPerOur = salaryPerOur
        self.workingOurs = workingOurs

    def salary(self):
        print(self.salaryPerOur * self.workingOurs)

class EmployeeMonthlyWorking2(Employee):
    def __init__(self, salaryPerOur, workingOurs):
        self.salaryPerOur = salaryPerOur
        self.workingOurs = workingOurs

    def salary(self):
        print(self.salaryPerOur * self.workingOurs)

class Team(Employee):


class SumSalary(self):
    def __init__(self, ):
        self.salaryPerOur = salaryPerOur
        self.workingOurs = workingOurs

    def total_salary(self):
        salary_total = 0
        for i in self.workers_list:
            salary_total += i.salary
        return salary_total

if __name__ == '__main__':
    employeeWeeklyWorking1 = EmployeeWeeklyWorking1(44, 40)
    employeeWeeklyWorking2 = EmployeeWeeklyWorking2(36, 38)
    employeeMonthlyWorking1 = EmployeeMonthlyWorking1(25, 160)
    employeeMonthlyWorking2 = EmployeeMonthlyWorking2(30, 140)
    sumSalary = SumSalary()

    employeeWeeklyWorking1.salary()
    employeeWeeklyWorking2.salary()
    employeeMonthlyWorking1.salary()
    employeeMonthlyWorking2.salary()
    sumSalary.salary_total()
